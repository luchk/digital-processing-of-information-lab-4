/* файл fft.c */
/* функція розраховує ДПФ послідовності */
/* з допомогою двійкового ШПФ */
/*---------------------------------------------------*/
void fft()
{
  int sign;
  long m,irem,l,le,le1,k,ip,i,j;
  double ur,ui,wr,wi,tr,ti,temp;
  extern long npt;
  extern complex x[size];
  /* перестановка із заміщенням даних, що обумовлена
  оберненим порядком бітів */
  j=1;
  for(i=1;i<npt;++i)
  {
    if(i<j)
    {
      tr=x[j].real;ti=x[j].imag;
      x[j].real=x[i].real;
      x[j].imag=x[i].imag;
      x[i].real=tr;x[i].imag=ti;
      k=npt/2;

      while(k<j)
      {
        j=j-k;
        k=k/2;
      }
    }
    else
    {
      k=npt/2;
      while(k<j)
      {
        j=j-k;
        k=k/2;
      }
    }
    j=j+k;

  }

  /* рахуємо кількість каскадів: m=log2(npt) і обираємо ШПФ
  або ОШПФ */
  m=0;irem=npt;
  while(irem>1)
  {
    irem=irem/2;
    m=m+1;
  }

  if(inv==1)
    sign=1;
  else
    sign=-1;

  /* рахуємо ШПФ для кожного каскаду*/
  for(l=1;l<=m,++l)
  {
    le=pow(2,l);
    le1=le/2;
    ur=1.0; ui=0;
    wr=cos(pi/le1);
    wi=sign*sin(pi/le1);
    for(j=1;j<=le1;++j)
    {
      i=j;
      while(i<=npt)
      {
        ip=i+le1;
        tr=x[ip].real*ur-x[ip].imag*ui;
        ti=x[ip].imag*ur+x[ip].real*ui;
        x[ip].real=x[i].real-tr;
        x[ip].imag=x[i].imag-ti;
        x[i].real=x[i].real+tr;
        x[i].imag=x[i].imag+ti;
        i=i+le;
      }
      temp=ur*wr-ui*wi;
      ui=ui*wr+ur*wi;
      ur=temp;
    }
  }

  /* якщо потрібно знайти ШПФ, то кожен коефіцієнт ділимо на на npt */

  if(inv=-1)
  {
    for(i=1;i<=npt;++i)
    {
      x[i].real=x[i].real/npt;
      x[i].imag=x[i].imag/npt;
    }
  }
}
