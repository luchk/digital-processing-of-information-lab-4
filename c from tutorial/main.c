/*--------------------------------------------*/
/* Лістинг програми ШПФ */
/*--------------------------------------------*/
#include <stdio.h>
#include <math.h>
#include <time.h>
SHPF(x,y,N,I) /*Процедура ШПФ */
register float *x,*y; /*х,у-вхідні масиви даних*/
register int N,I; /*розмірністю I=1 для ШПФ I=-1 для ОШПФ */
{
  register float с,s,t1,t2,t3,t4,u1,u2,u3;
  register int i,j,p,l,L,M,M1,K;
  L=N;
  M=N/2;
  M1=N-1;

  while(L>=2)
  {
    l=L/2; u1=1.; u2=0.; t1=PI/(float)l;
    c=cos(t1); s=(-1)*I*sin(t1);

    for(j=0; j<l;j++)
    {
      for(i=j;i<N;i+=L)
        {
          p=i+l;
          t1=*(x+i)+*(x+p);
          t2=*(y+i)+*(y+p);
          t3=*(x+i)-*(x+p);
          t4=*(y+i)-*(y+p);
          *(x+p)=t3*u1-t4*u2;
          *(y+p)=t4*u1+t3*u2;
          *(x+i)=t1; *(y+i)=t2;
        }
      u3=u1*c-u2*s;
      u2=u2*c+u1*s; u1=u3;
    }
    L/=2;
  }
  j=0;

  for(i=0;i<M1;i++)
  {
    if(i>j)
    {
      t1=*(x+j); t2=*(y+j);
      *(x+j)=*(x+i); *(y+j)=*(y+i);
      *(x+i)=t1; *(y+i)=t2;
    }
    K=M;

    while(j >=K)
    {
      j-=K;K/=2;
    }
    j+=K;
  }
}

sinsignal(PiFiAiN) /*моделювання вхідного сигналу*/

/*в формі синусоїди/*

float *P,F,A; /*Р-масив сигналу розмірності N*/
int N; /*F-частота сигналу, */
/*А-амплітуда сигналу*/
{
register int i;
register float r,re,re1,im,im1;
re=cos(2.*PI*F/(float)N);
im=sin(2.*PI*F/(float)N);
re1=A;im1=0.;
for(i=0;i< N;i++)
{
  *(P+i)=re1;r=re1;
  re1=r*re-im1*im;
  im1=im1*re+r*im;
}

}

main()
{
  int j,N;
  float *x,*y,F,A,Re,Im;
  printf("\t\t N :"); scanf("%d",&N);
  printf("\t\t F(gc):"); scanf("%f",&F);
  printf("\t\t A :"); scanf("%f",&A);
  x=(float*)calloc(N,sizeof(float));
  y=(float*)calloc(N,sizeof(float));
  sinsignal(x,F,A,N);
  for(j=0;j < N;j++)
    printf(" X[%d] - %.1f \n",j,*(x+j));
  SHPF(x,y,N,1);
  for(j=0;j < N/2;j++)
  {
    Re=*(x+j);
    Im=*(y+j);
    A=2.*sqrt(Re*Re+Im*Im)/(float)N;
    printf(" X[%d] - %d \n",j,(int)A);
    free(x); free(y);
  }
}

// Тестовий приклад
// N = 1024
// F = 100
// A = 100
// Вхідний масив :
// X[0]= 100
// X[1]= 81.8
// X[2]= 33.7
// X[3]= - 26.7
// X[4]= - 77.7
// X[5]= - 99.7
// ... ... ... ... ...
// X[1018]= - 85.8
// X[1019]= - 99.7
// X[1020]= - 77.3
// X[1021]= - 26.7
// X[1022]= 33.7
// X[1023]= 81.8
// Вихідний масив:
// X[0]= 0
// X[1]= 0
// X[2]= 0
// X[3]= 0
// X[4]= 0
// ...... ... ... ...
// X[100]= 100
// ... ... ... ... ...
// X[508]= 0
// X[507]= 0
// X[508]= 0
// X[509]= 0
// X[510]= 0
// X[511]= 0
